import rospy
from tf import TransformerROS, Transformer
import tf, os
import time
import numpy as np
import yaml

import numpy as np
import xml.etree.cElementTree as ET
import xml.dom.minidom

my_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(my_path)

class MapGridTransformation(object):
    def __init__(self):
        self.current_dir = "../config"
        self.c2m=np.load(self.current_dir+"/crop2map.npy")
        self.m2c=np.load(self.current_dir+"/map2crop.npy")
        with open(self.current_dir+"/generated_world_metadata.yaml", 'r') as stream:
            yaml_file = yaml.load(stream)
            self.resolution = yaml_file['resolution']#resolution is meters per pixel
            self.grid_size=yaml_file['grid_size']#grid size is in meters NOT pixels
            self.grid_pixel_size=self.grid_size/self.resolution

    def crop2map(self,loc):
        #loc is a (x,y) PIXEL tuple
        col_vec=np.array([[loc[0]],[loc[1]],[1]])
        result=self.c2m.dot(col_vec).flatten()
        return (result[0],result[1])

    def map2crop(self,loc):
        #loc is a (x,y) PIXEL tuple
        col_vec=np.array([[loc[0]],[loc[1]],[1]])
        result=self.m2c.dot(col_vec).flatten()
        return (result[0],result[1])
    def grid2map(self,location):
        r,c=location[0],location[1]
        crop_pixel_x,crop_pixel_y=(c+.5)*self.grid_pixel_size,(r+.5)*self.grid_pixel_size
        map_pixel_x,map_pixel_y=self.crop2map((crop_pixel_x,crop_pixel_y))
        map_x,map_y=map_pixel_x*self.resolution,map_pixel_y*self.resolution
        return (np.asscalar(map_x),np.asscalar(-map_y))
    def map2grid(self,location):
        map_x,map_y = location[0],-location[1]
        map_pixel_x,map_pixel_y=map_x/self.resolution,map_y/self.resolution
        crop_pixel_x,crop_pixel_y=self.map2crop((map_pixel_x,map_pixel_y))
        r,c=round(crop_pixel_x/self.grid_pixel_size-.5),round(crop_pixel_y/self.grid_pixel_size-.5)
        return (int(c),int(r))