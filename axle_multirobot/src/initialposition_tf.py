import rospy
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from nav_msgs.msg import Path, Odometry
#from rospy_message_converter import message_converter
from std_msgs.msg import String
from tf import TransformerROS, Transformer
import tf
import time
import yaml

import numpy as np
import xml.etree.cElementTree as ET
import xml.dom.minidom

class InitialPosition(object):

    # cd is the current directory you'd like to use for paths for npy files
    def __init__(self, cd = "../config"):
        self.current_dir = cd
        self.world = np.load(self.current_dir+"/environments/1.npy")
        self.c2m=np.load(self.current_dir+"/crop2map.npy")
        self.m2c=np.load(self.current_dir+"/map2crop.npy")
        with open(self.current_dir+"/generated_world_metadata.yaml", 'r') as stream:
            yaml_file = yaml.load(stream)
            self.resolution = yaml_file['resolution']#resolution is meters per pixel
            self.grid_size=yaml_file['grid_size']#grid size is in meters NOT pixels
            self.grid_pixel_size=self.grid_size/self.resolution
        # self.tflistener = rostf.TransformListener()
        self.agentState  = self.world[0] # world[0]: agent position and obs, world[1]: agent goal
        self.agentGoal = self.world[1]
        self.agent_number = np.max(self.agentState)
        self.multirobotfile = "..//launch/multirobot.launch"
        #self.rlplannerfile  = "../../mapf_navigation/launch/umstarPlanning.launch"

    def crop2map(self,loc):
        #loc is a (x,y) PIXEL tuple
        col_vec=np.array([[loc[0]],[loc[1]],[1]])
        result=self.c2m.dot(col_vec).flatten()
        return (result[0],result[1])

    def map2crop(self,loc):
        #loc is a (x,y) PIXEL tuple
        col_vec=np.array([[loc[0]],[loc[1]],[1]])
        result=self.m2c.dot(col_vec).flatten()
        return (result[0],result[1])

    def grid2map(self,location):
        r,c=location[0],location[1]
        crop_pixel_x,crop_pixel_y=(c+.5)*self.grid_pixel_size,(r+.5)*self.grid_pixel_size
        map_pixel_x,map_pixel_y=self.crop2map((crop_pixel_x,crop_pixel_y))
        map_x,map_y=map_pixel_x*self.resolution,map_pixel_y*self.resolution
        return (np.asscalar(map_x),np.asscalar(-map_y))

    def map2grid(self,location):
        map_x,map_y = location[0],location[1]
        map_pixel_x,map_pixel_y=map_x/self.resolution,map_y/self.resolution
        crop_pixel_x,crop_pixel_y=self.map2crop((map_pixel_x,map_pixel_y))
        r,c=round(crop_pixel_x/self.grid_pixel_size-.5),round(crop_pixel_y/self.grid_pixel_size-.5)
        return (int(c),int(r))

    def generateInitialPosition(self):
        initialPose = dict()
        for i in range(1, self.agent_number+1):
            gridPose = (np.where(self.agentState==i)[0],np.where(self.agentState==i)[1])
            #print(gridPose)
            mapPose = self.grid2map(gridPose)

            initialPose[i] = mapPose
        return initialPose
    
    def generateAgentGoals(self):
        goals = dict()
        for i in range(1, self.agent_number+1):
            goalPose = (np.where(self.agentGoal == i)[0], np.where(self.agentGoal==i)[1])
            #print(goalPose)
            mapGoalPose = self.grid2map(goalPose)

            goals[i] = mapGoalPose
        return goals

    def generateAgentInformation(self, number):
        
        #list that contains information about individual agentsi
        agentInformation = list()
        individualPosition = self.generateInitialPosition()
        individualGoals = self.generateAgentGoals()
        #x-position (Sx)
        agentInformation.append(individualPosition[number][0])
        #y-position (Sy)
        agentInformation.append(individualPosition[number][1])
        #goal-position (Gx)
        agentInformation.append(individualGoals[number][0])
        #goal-position (Gy)
        agentInformation.append(individualGoals[number][1])

        return agentInformation

    def getMaxAgentsList(self):

        agentList = list()
        for i in range(1, self.agent_number+1):
            agentList.append("/R" + str(i))

        return agentList

    def generateMultirobotLaunch(self):
        initialPose = self.generateInitialPosition()
        goals = self.generateAgentGoals()
        #print initialPose
        launch=ET.Element("launch")
        for i in range(1,self.agent_number+1):
            launch.insert(3*i,ET.Comment("ROBOT "+str(i)))
            group =ET.SubElement(launch, "group",ns="R"+str(i))
            ET.SubElement(group, "param", name = "tf_prefix", value="R"+str(i)+"_tf")
            include= ET.SubElement(group,"include", file="$(find axle_multirobot)/launch/one_robot.launch")
            ET.SubElement(include, "arg", name = "robot_name", value="R"+str(i))
            ET.SubElement(include, "arg", name = "Sx", value=str(initialPose[i][0]))
            ET.SubElement(include, "arg", name = "Sy", value=str(initialPose[i][1]))
            ET.SubElement(include, "arg", name = "Gx", value=str(goals[i][0]))
            ET.SubElement(include, "arg", name = "Gy", value=str(goals[i][1]))
        tree = ET.ElementTree(launch)
        tree.write(self.multirobotfile)
        temp = xml.dom.minidom.parse(self.multirobotfile)
        pretty = temp.toprettyxml()
        # print(pretty)
        with open(self.multirobotfile,'wb') as f:
            f.write(pretty)

    # def generateRLplannerLaunch(self):
    #     initialPose = self.generateInitialPosition()
    #     goals = self.generateAgentGoals()
    #     launch=ET.Element("launch")
    #     #ET.SubElement(launch, "node", name="goalMarker", pkg="mapf_navigation", type="publishMarker.py", output="screen")
    #     for i in range(1,self.agent_number+1):

    #         #node that launches the actionlib server for planning
    #         mstar = ET.SubElement(launch, "node", pkg="mapf_navigation", type="planner_ac_server.py", name="R"+str(i)+ "_planner", output="screen")
    #         ET.SubElement(mstar, "param", name="name", value="/R"+str(i))

    #         #node that launches each of our decentralized agents
    #         node = ET.SubElement(launch, "node", pkg = "mapf_navigation", type = "decen_mstar.py", name = "R" + str(i), output = "screen")
    #         ET.SubElement(node, "param", name = "robot_name", value="R"+str(i))
    #         ET.SubElement(node, "param", name = "Sx", value=str(initialPose[i][0]))
    #         ET.SubElement(node, "param", name = "Sy", value=str(initialPose[i][1]))
    #         ET.SubElement(node, "param", name = "Gx", value=str(goals[i][0]))
    #         ET.SubElement(node, "param", name = "Gy", value=str(goals[i][1]))

    #     tree = ET.ElementTree(launch)
    #     tree.write(self.rlplannerfile)
    #     temp = xml.dom.minidom.parse(self.rlplannerfile)
    #     pretty = temp.toprettyxml()
    #     print(pretty)
    #     with open(self.rlplannerfile,'wb') as f:
    #         f.write(pretty)


if __name__ == '__main__':
    generator = InitialPosition()
    generator.generateMultirobotLaunch()
    #generator.generateRLplannerLaunch()
